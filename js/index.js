/**
 * Created by IrisHuang on 2016/5/9.
 */
var skinschoise={
    defaultSkin:'css/indexdefault.css',
    skySkin:'css/indexsky.css',
    orangeSkin:'css/indexorange.css'
}
function switchSkin(skinName){
    $("#cssfile").attr("href",skinschoise[skinName]);
}

(function($){
    $(window).load(function(){
        $(".status-container").mCustomScrollbar({
            scrollInertia:1000
        });
    });

    var skinli=$("#skinMenu li");
    skinli.click(function(){
        switchSkin(this.id);
    });
})(jQuery);


